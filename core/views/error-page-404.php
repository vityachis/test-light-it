<?php
/**
 * Author:  Viktor Chistyakov
 * Nick:    vityachis
 * Email:   vityachis@ya.ru
 * VK:      https://vk.me/vityachis
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <h1 class="text-center">404 Страница не найдена</h1>
            </div>
        </div>
    </div>
</div>
