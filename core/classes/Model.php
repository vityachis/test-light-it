<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

namespace app\core\classes;

use PDO;
use app\core\classes\Application;

/**
 * Class Model
 */
class Model
{
    /**
     * @var string
     */
    private $nameModel;

    /**
     * Model constructor.
     * @param $nameModel
     */
    public function __construct($nameModel)
    {
        $this->nameModel = $nameModel;
    }

    /**
     * @return string
     */
    public function getNameModel()
    {
        return $this->nameModel;
    }
}
