<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

namespace app\core\classes;

use app\models\Users;
use PDO;

/**
 * Class Application
 */
class Application
{
    /**
     * @var string
     */
    private $layout;

    /**
     * @var PDO
     * TODO: Посмотреть что можно сделать с переменной, потому что она появляется только после создания объекта
     */
    public static $dbConnect;

    private static $systemConfig;

    private static $userInfo = false;

    /**
     * Application constructor.
     */
    public function __construct()
    {
        /** @var array $config */
        $this->layout = strtolower(self::systemConfig('main', 'defaultLayout'));
    }

    /**
     * Run application
     */
    public function run()
    {
        $route = new Route();
        $route->route();

        $this->connectDB();

        self::setUserInfo();

        $controllerName = "\\app\\controllers\\" . $route->getControllerName();
        if (file_exists(ROOT . '/controllers/' . $route->getControllerName() . '.php')) {
            $controller = new $controllerName($route->getActionPath(), $this->layout, $route->getDataParams());

            if (method_exists($controller, $route->getActionName())) {
                call_user_func([$controller, $route->getActionName()]);
            } else {
                Route::errorPage404($this->layout);
            }
        } else {
            Route::errorPage404($this->layout);
        }

    }

    /**
     * Connected database
     */
    public function connectDB()
    {
        $dbConfig = self::systemConfig('database');
        try {
            self::$dbConnect = new PDO($dbConfig['dsn'], $dbConfig['username'], $dbConfig['password']);
            if (self::systemConfig('debug') === 1) {
                self::$dbConnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
        } catch (PDOException $e) {
            die('PDO Error: ' . $e->getMessage());
        }
    }

    /**
     * Show config
     *
     * ```php
     * Application::systemConfig($arg1, $arg2, ..., $argN);
     * // ==
     * Application::systemConfig()[ $arg1 ][ $arg2 ][ ... ][ $argN ];
     * ```
     *
     * @return array
     */
    public static function systemConfig()
    {
        if (!isset(self::$systemConfig)) {
            self::$systemConfig = array_merge(
                require(CORE . '/config.php'),
                require(ROOT . '/config.php')
            );
        }

        $returnConfig = self::$systemConfig;
        foreach (func_get_args() as $arg) {
            $returnConfig = $returnConfig[ $arg ];
        }

        return $returnConfig;
    }

    public function getContent($template)
    {
        include "$template";
    }

    private static function setUserInfo()
    {
        if (!empty($_SESSION['id'])) {
            $model = new Users();
            self::$userInfo = $model->findOnePrimary($_SESSION['id']);
        }
    }

    public static function getUserInfo($key = false)
    {
        if ($key && self::$userInfo) {
            return isset(self::$userInfo[ $key ]) ? self::$userInfo[ $key ] : false;
        } else {
            return self::$userInfo;
        }
    }
}
