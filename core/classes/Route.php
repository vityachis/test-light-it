<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

namespace app\core\classes;

/**
 * Class Route
 */
class Route
{
    /**
     * @var string
     */
    private $controllerName;

    /**
     * @var string
     */
    private $actionName;

    /**
     * @var string
     */
    private $fullActionPath;

    /**
     * @var string
     */
    private $actionPathFolder;

    /**
     * @var string
     */
    private $actionPathFile;

    /**
     * @var array
     */
    private $dataParams = [];

    /**
     * Route constructor.
     */
    public function __construct()
    {
        /** @var array $config */
        $this->controllerName = Route::transformUrl(Application::systemConfig('main', 'defaultController')) . 'Controller';

        $this->actionPathFolder = strtolower(Application::systemConfig('main', 'defaultController'));
        $this->actionPathFile = strtolower(Application::systemConfig('main', 'defaultAction'));
        $this->actionName = 'action' . Route::transformUrl(Application::systemConfig('main', 'defaultAction'));
    }

    /**
     * Run route
     */
    public function route()
    {
        $url = explode('/', $_SERVER['REQUEST_URI']);

        if (!empty($url[1])) {
            $this->controllerName = self::transformUrl($url[1]) . 'Controller';
            $this->actionPathFolder = $url[1];
        }

        if (!empty($url[2])) {

            $urlParam = explode('?', $url[2]);
            $this->actionPathFile = $urlParam[0];

            if (count($urlParam) >= 2) {
                $getData = explode('&', $urlParam[1]);

                $dataParams = [];

                foreach ($getData as $item) {
                    $val = explode('=', $item);
                    if (!isset($val[1])) {
                        continue;
                    }

                    $dataParams[ $val[0] ] = $val[1];
                }
                $this->dataParams = $dataParams;
            }

            $this->actionName = 'action' . self::transformUrl($this->actionPathFile);
        }

        $this->fullActionPath = $this->actionPathFolder . '/' . $this->actionPathFile;
    }

    /**
     * Transform URL to name files
     *
     * @param string $url
     * @return string
     */
    public static function transformUrl($url)
    {
        $transform = explode('-', $url);
        $result = '';
        foreach ($transform as $partUrl) {
            $result .= ucfirst($partUrl);
        }

        return $result;
    }

    /**
     * Show error page 404
     *
     * @param $layout
     */
    public static function errorPage404($layout)
    {
        (new View($layout))->generate(Application::systemConfig('main', 'errorPage404'), ['title' => '404: Not Found']);
        exit();
    }

    /**
     * @return string
     */
    public function getControllerName()
    {
        return $this->controllerName;
    }

    /**
     * @return string
     */
    public function getActionName()
    {
        return $this->actionName;
    }

    /**
     * @return string
     */
    public function getActionPath()
    {
        return $this->fullActionPath;
    }

    /**
     * @return array
     */
    public function getDataParams()
    {
        return $this->dataParams;
    }
}
