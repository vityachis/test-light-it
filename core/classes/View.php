<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

namespace app\core\classes;

/**
 * Class View
 */
class View
{
    /**
     * @var string
     */
    private $layout;

    /**
     * View constructor.
     *
     * @param $layout
     */
    public function __construct($layout)
    {
        $this->layout = $layout;
    }

    /**
     * Page generate
     *
     * @param array $content
     * Error view:
     * ```php
     * 'alertMessageList' => [
     *      [
     *          'type' => 'danger',
     *          'name' => 'Error',
     *          'description' => 'Text Error!',
     *      ],
     * ],
     * ```
     * @param string $urlTemplate
     */
    public function generate($urlTemplate, $content = [])
    {
        if (!file_exists(ROOT . '/views/' . $urlTemplate . '.php')) {
            $template = CORE . '/views/' . $urlTemplate . '.php';
        } else {
            $template = ROOT . '/views/' . $urlTemplate . '.php';
        }

        include(ROOT . '/views/layouts/' . $this->layout . '.php');
    }
}
