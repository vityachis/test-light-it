<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

namespace app\core\classes;

use PDO;

class ActiveRecord extends Model
{
    const SORT_DESC = 'DESC';
    const SORT_ASC = 'ASC';

    /**
     * @var PDO
     */
    private $dbConnect;

    /**
     * ActiveRecord constructor.
     *
     * @param string $nameTable
     * @internal param PDO $dbConnect
     */
    public function __construct($nameTable)
    {
        parent::__construct($nameTable);
        $this->dbConnect = Application::$dbConnect;
    }

    /**
     * @param string $sort
     * @param int $limit
     * @return array
     */
    public function findAll($sort = self::SORT_ASC, $limit = 25)
    {
        return $this->dbConnect->query('SELECT * FROM ' . $this->getNameTable() . ' ORDER BY id ' . $sort . ' LIMIT ' . $limit)->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param array $condition
     * @param string $sort
     * @param int $limit
     * @return array
     */
    public function findAllCondition($condition, $sort = self::SORT_ASC, $limit = 25)
    {

        if (is_array($condition) && count($condition) >= 1) {
            $params = '';
            $unite = false;
            foreach ($condition as $key => $val) {
                if ($unite) {
                    $params .= ' AND ' . $key . ' = :' . $key;
                } else {
                    $unite = true;
                    $params .= $key . ' = :' . $key;
                }
            }
        }

        $stmt = $this->dbConnect->prepare('SELECT * FROM ' . $this->getNameTable() . ' WHERE ' . $params . ' ORDER BY id ' . $sort . ' LIMIT ' . $limit);
        foreach ($condition as $key => $val) {
            $stmt->bindValue(':' . $key, $val);
        }
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function findOnePrimary($id)
    {
        $stmt = $this->dbConnect->prepare('SELECT * FROM ' . $this->getNameTable() . ' WHERE id = :id');
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $column
     * @param int|string|bool $value
     * @param int $typeValue
     * @return mixed
     */
    public function findOneUnique($column, $value, $typeValue = PDO::PARAM_INT)
    {
        $stmt = $this->dbConnect->prepare('SELECT * FROM ' . $this->getNameTable() . ' WHERE `' . $column . '` = :value');
        $stmt->bindValue(':value', $value, $typeValue);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }


    /**
     * @param array $data
     * @param bool|string $cr_date
     * @param bool|string $user_id
     * @param bool $guest
     * @return bool
     */
    public function add($data, $cr_date = false, $user_id = false, $guest = false)
    {
        if ($guest || Application::getUserInfo()) {
            $params = '';
            $unite = false;
            $time = time();
            $id = Application::getUserInfo('id');

            foreach ($data as $key => $val) {
                if ($unite) {
                    $params .= ', ' . $key . ' = :' . $key;
                } else {
                    $unite = true;
                    $params .= $key . ' = :' . $key;
                }
            }

            if ($cr_date === true) {
                $params .= ', cr_date = :cr_date';
            } elseif (is_string($cr_date)) {
                $params .= $cr_date . ' = :' . $cr_date;
            }

            if ($user_id === true) {
                $params .= ', user_id = :user_id';
            } elseif (is_string($user_id)) {
                $params .= $user_id . ' = :' . $user_id;
            }

            $stmt = $this->dbConnect->prepare('INSERT INTO ' . $this->getNameTable() . ' SET ' . $params);

            foreach ($data as $key => $val) {
                $stmt->bindParam(':' . $key, $data[ $key ]);
            }

            if ($cr_date === true) {
                $stmt->bindParam(':cr_date', $time);
            } elseif (is_string($cr_date)) {
                $stmt->bindParam(':' . $cr_date, $time);
            }

            if ($user_id === true) {
                $stmt->bindParam(':user_id', $id);
            } elseif (is_string($cr_date)) {
                $stmt->bindParam(':' . $user_id, $id);
            }

            return $stmt->execute();
        }

    }

    /**
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function update($id, $data)
    {
        $params = '';
        $comma = false;
        foreach ($data as $key => $val) {
            if ($comma) {
                $params .= ', ' . $key . ' = :' . $key;
            } else {
                $comma = true;
                $params .= $key . ' = :' . $key;
            }
        }

        $stmt = $this->dbConnect->prepare('UPDATE ' . $this->getNameTable() . ' SET ' . $params . ' WHERE id = :id');

        foreach ($data as $key => $val) {
            $stmt->bindParam(':' . $key, $data[ $key ], PDO::PARAM_STR);
        }
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);

        return $stmt->execute();
    }


    /**
     * @param int $id
     * @return bool
     */
    public function delete($id)
    {
        $stmt = $this->dbConnect->prepare('DELETE FROM ' . $this->getNameTable() . ' WHERE id = :id LIMIT 1');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        return $stmt->execute();
    }


    /**
     * @return string
     */
    public function getNameTable()
    {
        return $this->getNameModel();
    }
}