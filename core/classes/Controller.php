<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

namespace app\core\classes;

/**
 * Class Controller
 */
class Controller
{
    /**
     * @var View
     */
    protected $view;

    /**
     * @var string
     */
    protected $actionPath;

    /**
     * @var array
     */
    protected $listName = [];

    /**
     * @var array
     */
    protected $modelList = [];

    /**
     * @var array
     */
    private $dataParams = [];

    /**
     * Controller constructor.
     *
     * @param string $actionPath
     * @param string $layout
     * @param array $dataParams
     */
    public function __construct($actionPath, $layout, $dataParams)
    {
        $this->view = new View($layout);
        $this->actionPath = $actionPath;
        $this->dataParams = $dataParams;

        foreach ($this->listName as $name) {
            $modelName = Route::transformUrl($name);
            $modelNamespace = "\\app\\models\\" . Route::transformUrl($name);
                $this->modelList[ strtolower($name) ] = new $modelNamespace($modelName);
        }
    }

    /**
     * Getter object model
     *
     * @param $key
     * @return Model
     */
    public function getModel($key)
    {
        return $this->modelList[ strtolower($key) ];
    }

    /**
     * @param string $key
     * @param bool $typeInt
     * @return int|string|bool
     */
    public function getDataParam($key, $typeInt = false)
    {
        if ($typeInt) {
            return isset($this->dataParams[ $key ]) ? (int) $this->dataParams[ $key ] : false;
        } else {
            return isset($this->dataParams[ $key ]) ? $this->dataParams[ $key ] : false;
        }

    }

    /**
     * @return array
     */
    public function getDataParams()
    {
        return $this->dataParams;
    }
}
