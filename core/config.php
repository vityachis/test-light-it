<?php
/**
 * Author:  Viktor Chistyakov
 * Nick:    vityachis
 * Email:   vityachis@ya.ru
 * VK:      https://vk.me/vityachis
 */

return [
    'main' => [
        'defaultController' => 'default',
        'defaultAction' => 'index',
        'defaultLayout' => 'main',
        'errorPage404' => 'error-page-404',
        'nameProject' => 'vityachis',
    ],
    'debug' => 0,
];
