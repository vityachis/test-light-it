<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

use app\core\classes\Application;
/** @var array $content */

$user = Application::getUserInfo();
?>

<form action="/default/index" name="message" id="form_message" method="POST">
    <input type="hidden" id="sub_message" name="sub_message" value="0">
    <input type="hidden" id="sub_comment" name="sub_comment" value="0">
    <div class="form-group">
        <textarea name="text_message" class="form-control" rows="3" onKeyDown="sendMessage(event)" placeholder="<?= $user ? 'Введите текст сообщения...' : 'Для начала войдите через одну из соцсетей...'; ?>" <?= $user ? '' : 'title="Для начала войдите через одну из соцсетей..." disabled'; ?> minlength="10" required></textarea>
    </div>
    <div class="row">
        <?php if (!$user): ?>
        <div class="col-md-10">
            <strong class="text-danger">Авторизируйтесь с помощью профиля в соцсети:</strong> <a class="btn btn-primary" href="/default/vk-auth">Войти через <span class="fa fa-vk" aria-hidden="true"></span></a>
        </div>
        <?php endif; ?>
        <div class="col-md-<?= $user ? '12' : '2'; ?>">
            <div class="form-group text-right btn-submit">
                <input type="submit" id="send_message" class="btn btn-primary" value="Отправить" <?= $user ? '' : 'title="Для начала войдите через одну из соцсетей..." disabled'; ?>>
            </div>
        </div>
    </div>
</form>

<h2 class="page-header">Список сообщений: </h2>
<?php if (isset($content['data']) && count($content['data']) >= 1): ?>
<div class="list-group">
    <?php foreach ($content['data'] as $item): ?>
        <span data-id="<?= $item['id']; ?>" data-level="0" class="list-group-item message-item">[<?= date('d.m.Y', $item['cr_date']); ?>] <span class="text_message"><?= $item['text_message']; ?></span></span>
        <div id="mess-sub-<?= $item['id']; ?>" class="sub"></div>
    <?php endforeach; ?>
</div>
<?php else: ?>
<div class="alert alert-danger text-center">Сообщения отсутствуют!</div>
<?php endif; ?>
