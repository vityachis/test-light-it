<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

/** @var array $content */
?>
<h2 class="page-header">
    <span><?= $content['title'] ?></span>
</h2>

<div class="row">
    <div class="col-md-2 col-md-offset-5">
        <a class="btn btn-primary" href="/default/vk-auth">Войти через <span class="fa fa-vk" aria-hidden="true"></span></a>
    </div>
</div>