<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

use app\core\classes\Application;

/** @var string $template */
/** @var string $urlTemplate */
/** @var array $content */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= RESOURCES ?>img/favicon.ico">

    <title><?= $content['title'] ?></title>
    <link href="<?= RESOURCES ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= RESOURCES ?>css/font-awesome.min.css" rel="stylesheet">
    <link href="<?= RESOURCES ?>css/main.css" rel="stylesheet">
</head>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed"
                        data-toggle="collapse"
                        data-target="#navbar"
                        aria-expanded="false"
                        aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><?= Application::systemConfig('main', 'nameProject') ?></a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="<?= $urlTemplate == 'default/index' ? 'active' : '' ?>"><a href="/">Главная</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php if (Application::getUserInfo()): ?>
                        <li><a href="#"><?= Application::getUserInfo('first_name') ?> <?= Application::getUserInfo('last_name') ?></a></li>
                        <li class="log-out"><a href="/default/log-out">Выход</a></li>
                    <?php else: ?>
                        <li class="sign-in <?= $urlTemplate == 'default/sign-in' ? 'active' : '' ?>"><a href="/default/sign-in" <?= $urlTemplate == 'default/sign-in' ? 'onClick="return false;"' : '' ?>">Авторизация</a></li>
                    <?php endif; ?>

                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if (isset($content['alertMessageList'])): ?>
                    <?php foreach ($content['alertMessageList'] as $errorMessage): ?>
                        <div class="text-center alert alert-<?= $errorMessage['type'] ?>">
                            <strong><?= $errorMessage['name'] ?>!</strong> <?= $errorMessage['description'] ?>
                        </div>
                    <?php endforeach ?>
                <?php endif ?>

                 <?php include("$template"); ?>

            </div>
        </div>
    </div>

    <script src="<?= RESOURCES ?>js/jquery.min.js"></script>
    <script src="<?= RESOURCES ?>js/bootstrap.min.js"></script>
    <script src="<?= RESOURCES ?>js/script.js"></script>
</body>
</html>
