/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

function sendMessage(e) {
    e = e || window.event;
    if (e.keyCode == 13 && e.ctrlKey) {
        document.getElementById('form_message').submit();
    };
};

$(document).ready(function() {

    var selectItem = function() {
        switch ($(this).data('level')) {
            case 0:
                $('.message-item').removeClass('list-group-item-info');
                $('.comment-item').removeClass('list-group-item-success');
                $('.message-item.active').removeClass('active');
                $('.comment-item.active').removeClass('active');
                $('.message-item').next().empty();
                break;
            case 1:
                $('.comment-item').removeClass('list-group-item-success');
                $('.message-item.active').removeClass('active');
                $('.comment-item.active').removeClass('active');
                $('.comment-item').next().empty();
                break;
            case 2:
                return false;
                break;
        }
        $('#sub_message, #sub_comment').val(0);

        $.ajax({
            url: '/default/ajax-pull-comments',
            data: {
                id: $(this).data('id'),
                level: $(this).data('level'),
            },
            type: 'GET',
            dataType: 'html',
            elem: $(this),
        }).done(function(data) {
            this.elem.addClass('list-group-item-info');
            if (this.elem.data('level') === 0) {
                this.elem.addClass('list-group-item-info');
                this.elem.addClass('active');
                $('#mess-sub-' + this.elem.data('id')).html(data);
            } else if (this.elem.data('level') === 1) {
                this.elem.addClass('list-group-item-success');
                this.elem.addClass('active');
                $('#comm-sub-' + this.elem.data('id')).html(data);
            }

            $('.comment-item').off('click');
            $('.comment-item').on('click', selectItem);
            if (this.elem.data('level') === 0) {
                $('#sub_message').val(this.elem.data('id'));
            } else if (this.elem.data('level') === 1) {
                $('#sub_comment').val(this.elem.data('id'));
            }
        });
    }

    $('.message-item').on('click', selectItem);
});