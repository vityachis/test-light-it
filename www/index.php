<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

session_start();

// Вывод ошибок настраивается в файле ../config.php['debug']
ini_set('display_errors', 1);

define('ROOT', dirname(__FILE__) . '/..');
define('CORE', dirname(__FILE__) . '/../core');
define('RESOURCES', '/');

require(ROOT . '/vendor/autoload.php');
ini_set('display_errors', (int) \app\core\classes\Application::systemConfig('debug'));

(new \app\core\classes\Application())->run();
