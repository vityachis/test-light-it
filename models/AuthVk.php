<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

namespace app\models;

use app\core\classes\Application;
use app\core\classes\Model;

class AuthVk extends Model
{
    private $clientID;
    private $clientKey;
    private $redirectUri;
    private $authorize;
    private $access_token;
    private $method;

    public function __construct($nameModel)
    {
        parent::__construct($nameModel);
        $this->clientID = Application::systemConfig('vkApi', 'id');
        $this->clientKey = Application::systemConfig('vkApi', 'key');
        $this->redirectUri = Application::systemConfig('vkApi', 'uri');
        $this->authorize = Application::systemConfig('vkApi', 'authorize');
        $this->access_token = Application::systemConfig('vkApi', 'access_token');
        $this->method = Application::systemConfig('vkApi', 'method');
    }

    public function getCode()
    {
        $params = [
            'client_id'     => $this->clientID,
            'redirect_uri'  => $this->redirectUri,
            'response_type' => 'code'
        ];

        return $this->authorize . '?' . urldecode(http_build_query($params));
    }

    public function getToken($code) {
        $params = [
            'client_id' => $this->clientID,
            'client_secret' => $this->clientKey,
            'code' => $code,
            'redirect_uri' => $this->redirectUri,
        ];

        $token = json_decode(file_get_contents($this->access_token . '?' . urldecode(http_build_query($params))), true);

        if (isset($token['access_token'])) {
            $params = [
                'uids' => $token['user_id'],
                'fields' => 'uid,first_name,last_name,screen_name,sex,bdate,photo_big',
                'access_token' => $token['access_token']
            ];

            $userInfo = json_decode(file_get_contents($this->method . 'users.get?' . urldecode(http_build_query($params))), true);
            if (!empty($userInfo['response'][0])) {
                $model = new Users();
                $user = $model->findOneUnique('uid', $userInfo['response'][0]['uid']);
                if (empty($user)) {
                    $data = [];
                    foreach ($userInfo['response'][0] as $key => $val) {
                        $data[$key] = $val;
                    }
                    $user = $model->add($data, true);
                    $_SESSION['id'] = $user['id'];
                } else {
                    $_SESSION['id'] = $user['id'];
                }
                header('Location: /default/index');
            }
        }
    }
}