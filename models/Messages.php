<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

namespace app\models;

use app\core\classes\ActiveRecord;

class Messages extends ActiveRecord
{
    public function __construct($nameTable = 'messages')
    {
        parent::__construct($nameTable);
    }
}