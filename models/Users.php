<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

namespace app\models;

use app\core\classes\ActiveRecord;
use PDO;

/**
 * Class Users
 */
class Users extends ActiveRecord
{
    public function __construct($nameTable = 'users')
    {
        parent::__construct($nameTable);
    }
}
