<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

namespace app\models;

use app\core\classes\ActiveRecord;

class Comments extends ActiveRecord
{
    public function __construct($nameTable = 'comments')
    {
        parent::__construct($nameTable);
    }
}