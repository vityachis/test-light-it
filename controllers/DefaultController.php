<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

namespace  app\controllers;

use app\core\classes\Application;
use app\core\classes\Controller;
use app\models\Messages;

/**
 * Class DefaultController
 */
class DefaultController extends Controller
{
    /**
     * DefaultController constructor.
     *
     * @param string $actionPath
     * @param string $layout
     * @param array $dataParams
     */
    public function __construct($actionPath, $layout, $dataParams)
    {
        $this->listName = [
            'users',
            'authVk',
            'messages',
            'comments',
        ];

        parent::__construct($actionPath, $layout, $dataParams);
    }

    /**
     * Index action
     */
    public function actionIndex()
    {
        if (isset($_POST) && isset($_POST['text_message']) && strlen($_POST['text_message']) >= 10) {
            $subMessage = (int) $_POST['sub_message'];
            $subComment = (int) $_POST['sub_comment'];
            if ($subMessage === 0 && $subComment === 0) {

                $data = [
                    'text_message' => $_POST['text_message'],
                ];

                $this->getModel('messages')->add($data, true, true);

            } else {
                if ($subMessage === 0 XOR $subComment === 0) {
                    if ($subMessage !== 0) {
                        $data = [
                            'answer_id' => $subMessage,
                            'text_comment' => $_POST['text_message'],
                            'for_message' => 1,
                        ];
                        $this->getModel('comments')->add($data, true, true);
                    }
                    if ($subComment !== 0) {
                        $data = [
                            'answer_id' => $subComment,
                            'text_comment' => $_POST['text_message'],
                            'for_message' => 0,
                        ];
                        $this->getModel('comments')->add($data, true, true);
                    }
                }
            }

            header("Location: /default/index");
        }

        $data = $this->getModel('messages')->findAll(Messages::SORT_DESC);

        $this->view->generate($this->actionPath, [
            'title' => 'Главная страница',
            'data' => $data,
        ]);
    }
    public function actionAjaxPullComments()
    {
        $result = '';
        $id = $this->getDataParam('id', true);
        $level = $this->getDataParam('level', true);

        if ($level === 0) {
            $data = $this->getModel('comments')->findAllCondition(['answer_id' => $id, 'for_message' => '1']);
        } else if ($level === 1){
            $data = $this->getModel('comments')->findAllCondition(['answer_id' => $id, 'for_message' => '0']);
        } else {
            return;
        }

        if ($data) {
            foreach ($data as $item) {
                if ($item['for_message'] === '1') {
                    $result .= '<span data-id="' . $item['id'] . '" data-level="1" class="list-group-item list-group-item-' . (($level === 0) ? 'info' : 'success') . ' comment-item"><i class="fa fa-minus" aria-hidden="true"></i> &nbsp; [' . date('d.m.Y', $item['cr_date']) . '] <span class="text_comment">' . $item['text_comment'] . '</span></span>';
                    $result .= '<div id="comm-sub-' . $item['id'] . '" class="sub"></div>';
                } else {
                    $result .= '<span data-id="' . $item['id'] . '" data-level="2" class="list-group-item list-group-item-' . (($level === 0) ? 'info' : 'success') . ' comment-item"><i class="fa fa-minus" aria-hidden="true"></i> <i class="fa fa-minus" aria-hidden="true"></i> &nbsp; [' . date('d.m.Y', $item['cr_date']) . '] <span class="text_comment">' . $item['text_comment'] . '</span></span>';
                }
            }
        }

        echo $result;
    }

    public function actionSignIn()
    {
        if (Application::getUserInfo()) {
            header("Location: /default/index");
        }

        $this->view->generate($this->actionPath, [
            'title' => 'Авторизация / Регистрация',
        ]);
    }

    public function actionVkAuth()
    {
        if (Application::getUserInfo()) {
            header("Location: /default/index");
        }

        if (!empty($this->getDataParam('code'))) {
            $this->getModel('authVk')->getToken($this->getDataParam('code'));
        } else {
            header('Location: ' . $this->getModel('authVk')->getCode());
        }
    }

    public function actionLogOut()
    {
        unset($_SESSION);
        session_destroy();
        header("Location: /default/index");
    }
}
