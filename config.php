<?php
/**
 * Author:   Viktor Chistyakov
 * Nick:     vityachis
 * Email:    vityachis@ya.ru
 * Telegram: https://t.me/vityachis
 * VK:       https://vk.me/vityachis
 */

return [
    'debug' => 1,
    'vkApi' => [
        'id' => '6126684',
        'key' => 'prRku3nCFxFFA1MIcwa4',
        'uri' => 'http://light.lc/default/vk-auth',
        'authorize' => 'https://oauth.vk.com/authorize',
        'access_token' => 'https://oauth.vk.com/access_token',
        'method' => 'https://api.vk.com/method/'

    ],
    'admin' => [
        'name' => 'Viktor Chistyakov',
        'email' => 'vityachis@yandex.com',
    ],
    'database' => [
        'dsn' => 'mysql:host=localhost;dbname=light',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',
    ],
];
